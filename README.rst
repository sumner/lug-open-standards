Open Standards
##############
A Presentation About Open Standards for LUG
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Authors/Presenters
------------------

- `Sumner Evans`_
- `Robby Zampino`_

.. _Sumner Evans: https://gitlab.com/sumner
.. _Robby Zampino: https://gitlab.com/robozman

Topics Covered
--------------

- What is a Standard?
- What is an Open Standard?

  - Advantages

- Examples:

  - Infrastructure

    @robozman

  - Application Level

    - Industry (B2B)

      - IAB
      - Bank Check21

    - Consumer

      - Matrix
      - Subsonic
